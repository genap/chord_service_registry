# CHORD Service Registry

**Author:** David Lougheed, Canadian Centre for Computational Genomics

Prototype implementation of GA4GH's [service registry API](https://github.com/ga4gh-discovery/ga4gh-service-registry/)
for the CHORD project.
